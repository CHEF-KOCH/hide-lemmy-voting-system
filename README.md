# Hide Lemmy's Up- Downvote System

The custom filter list hides Lemmy's entire voting system, it should work with uBlock, AdGuard and Brave Ad Block.


## Info

Lemmy has an option to disable the voting system in your settings, however you still see the up and downvote arrors among small indicators. 

Also, if you reload the page on desktop you might still see the entire voting system until the page is fully loaded.


## Drawbacks

- Only works and tested on Lemmy.ml it is not designed to support other Instances.
- Might not work with all ad-blocking systems, I only tested AdGuar, Brave and uBlock.


### RegEX filter

I had no time to look into it.
